<?php
$paginas = [
    "Home"=>'paginas/html/home.html', 
    "Sobre"=>"paginas/html/sobre.html",

    "cpf_gera"=>"paginas/php/gerador_cpf.php",
    "cpf_valida"=>"paginas/php/validador_cpf.php",
    "cnpj_gera"=>"paginas/php/gerador_cnpj.php",
    "cnpj_valida"=>"paginas/php/validador_cnpj.php",
    
    "Recomende"=>"paginas/php/recomende/recomende.php",
    "recomende_inserir"=>"paginas/php/recomende/recomende_inserir.php",
    "recomende_alterar"=>"paginas/php/recomende/recomende_alterar.php",
    "recomende_ver"=>"paginas/php/recomende/recomende_ver.php",

    "Login"=>"paginas/php/login.php",
    "Cadastro"=>"paginas/php/cadastro.php",
    "Dashboard"=>"paginas/php/dashboard.php",
    "Cotacao"=>"paginas/php/cotacao.php",
    "Previsao_tempo"=>"paginas/php/previsao_tempo.php",
    "Feedback"=>"paginas/php/feedback.php"
        ];
$pagina = 0;

if (isset($_GET['$pagina'])){
    $pagina = $_GET['$pagina'];
}


?>


<!doctype html>
<html lang="pt-br">

<head>
    <?php
    echo "<title>MP site</title>";
    require_once "parciais/_head.html";
    ?>
</head>

<body>

<header>
    <?php
        require_once "parciais/_nav.html";
    ?>
</header>

<?php
switch($pagina){
    case "Home":
        require_once $paginas["Home"];
        break;

    case "Sobre":
        require_once $paginas["Sobre"];
        break;

    case "Gerador de CPF":
        require_once $paginas["cpf_gera"];
        break;

    case "Validador de CPF":
        require_once $paginas["cpf_valida"];
        break;

    case "Gerador de CNPJ":
        require_once $paginas["cnpj_gera"];
        break;

    case "Validador de CNPJ":
        require_once $paginas["cnpj_valida"];
        break;

    case "Recomende":
        require_once $paginas["Recomende"];
        break;

    case "Inserir Recomende":
        require_once $paginas["recomende_inserir"];
        break;

    case "Alterar Recomende":
        require_once $paginas["recomende_alterar"];
        break;

    case "Ver Recomende":
        require_once $paginas["recomende_ver"];
        break;

    case "Login":
        require_once $paginas["Login"];
        break;

    case "Cadastro":
        require_once $paginas["Cadastro"];
        break;

    case "Dashboard":
        require_once $paginas["Dashboard"];
        break;

    case "Cotacao":
        require_once $paginas["Cotacao"];
        break;

    case "Previsao do tempo":
        require_once $paginas["Previsao_tempo"];
        break;


    
    case "Feedback":
        require_once $paginas["Feedback"];
        break;

    default:
        require_once "paginas/html/home.html";
        break;
}
?>





<footer>
    <script src="vendor/jquery/jquery.slim.min.js" rel="stylesheet"></script>
    <script src="vendor/bootstrap-5.0.2-dist/js/bootstrap.bundle.min.js" rel="stylesheet"></script>
</footer>

</body>

</html>