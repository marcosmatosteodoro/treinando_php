<?php
function conectar(){
    // IP do BD, Usuário, Senha, Nome do banco
    $conn = new mysqli("localhost", "root", "", "recomende_php");

    if ($conn->connect_error){
        echo "Error: " . $conn->connect_error;
    }
    return $conn;
}

function inserir_recomende($Titulo, $Resenha, $categoria, $opiniao, $nome_img){
    $data_criacao = date("Y/m/d");
    $Mostrar = 1;

    $conn = conectar();
    $stmt = $conn->prepare("INSERT INTO recomende (Titulo, Mostrar, Resenha, data_criacao, categoria, opiniao, nome_img) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssdsss", $Titulo, $Mostrar, $Resenha, $data_criacao,  $categoria, $opiniao, $nome_img);
    $stmt->execute();
}

function ver_recomende(){

    $conn = conectar();

    $result = $conn->query("SELECT * FROM recomende ORDER BY Titulo");
    
    //para transformar resuktado em json
    $data = array();
    
    //->fetch_array retorna se houver dados
    while ($row = $result->fetch_array(MYSQLI_ASSOC)){
    
        array_push($data, $row);
    }
    return $data;
    }


    ?>