
<div class="container h-100" style="width: 70%; background-color:">
    <div class="col" style="max-width: 800px; min-width: 400px">
        <div class="card">
            <div class="card-header">
                <h1>Gerador de CNPJ</h1>
            </div>
            <p style="margin: 1% 2% 0 2%">
                Neste projeto me propus a criar um simples programa que gera CNPJs numericamente válidos de acordo com o
                ministério da fazenda com limitação de 10 requisições na intenção de exercitar os códigos<br>
                Vale ressaltar que não foi utilizado nenhum código já pronto para este projeto
            </p> 
            <hr>
            <div class="card-body">
                <form method="GET" action="">
                    <div class="mb-3">
                        <div class="row">
                            <div class="col-6">
                                <div class="input-group">
                                    <label  class="imput-group-text" style="width:100px">Quantidade:</label>
                                    <input type="number" name="value"  value="<?php  if (isset($_GET['value'])){echo $_GET['value'];}; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-2">
                                <input type="checkbox" class="form-check-input" name="formata" value="true" checked>Formata<br>
                            </div>
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary"  name="$pagina" value="Gerador de CNPJ">Gerar CNPJ</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row" >
            <?php

                require_once "paginas/funcoes/validador_e_gerador_CNPJ.php";

                if (isset($_GET['value'])){
                    $value = (int)$_GET['value'];
                    $formata = false;
                    if (isset($_GET['formata'])){
                        $formata = true;
                    }
                    for ($i = 1; $i <= $value; $i++) {
                        if ($i > 10){
                            echo "<div class='card' style='width:800px; min-width:400px; padding-bootom:1%;'>";
                            echo "<div class='card-body btn-danger text-center'>";
                            echo "Limite de 10 CNPJs gerados alcançado";
                            echo "</div>";
                            echo "</div>";
                            break;
                        }

                        $result= geraCNPJ($formata);
                        echo "<div class='card' style='width:400px; min-width:400px; padding-bootom:1%;background-color:'>";
                        echo "<div class='card-body btn-success text-center'>";
                        echo "$result";
                        echo "</div>";
                        echo "</div>";

                        
                        }
                }   
            ?>
        </div>
    </div>
    <br>
    <div style="max-width:800px">
        <h4 class="text-center" > Veja o código abaixo</h4>
        <div style="background-color:rgb(30,30,30);">    
            <img style='min-width:400px'src="vendor/img2/codigos/gera_cnpj_img1.jpg">
            <img style='min-width:400px' src="vendor/img2/codigos/gera_cnpj_img2.jpg">
        </div>
    </div>


</div>