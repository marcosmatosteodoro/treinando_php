<?php




require_once "paginas\Mysql/recomende_bd.php";

$error = false;

if(isset($_POST["resenha"])){
    if(isset($_POST["titulo"]) === false | (isset($_POST["resenha"]) === false)){
        $msg = "Nenhum campo pode ser vazio";
        $tipo = "danger";
        $erro = true;

    }
    if(isset($_POST["opiniao"]) === false | (isset($_POST["categoria"]) === false)){
        $msg = "Nenhum campo pode ser vazio";
        $tipo = "danger";
        $error = true;

    }
    if (isset($_FILES['capa'])){
        $extensao = strtolower(substr($_FILES['capa']['name'], -4));
        $nome_img = md5(time()) . $extensao;
        $diretorio = "upload/recomende/";

        move_uploaded_file($_FILES['capa']['tmp_name'], $diretorio . $nome_img);
    }
    else{
        $msg = "Nenhum campo pode ser vazio";
        $tipo = "danger";
        $error = true;
    }

    if ($error == false){
        $titulo = $_POST["titulo"];
        $resenha = $_POST["resenha"];
        $opiniao = $_POST["opiniao"];
        $categoria = $_POST["categoria"];


        inserir_recomende($titulo, $resenha, $categoria, $opiniao, $nome_img);
        $msg = "$titulo salvo com sucesso";
        $tipo ="success";
    }
}

?>

<div class='container'>
    <h3>Faça uma recomendação</h3>
    <?php 
    if(isset($msg)){
     echo "<div class='card'padding-bootom:1%;background-color:'>";
     echo "<div class='card-body btn-$tipo text-center'>";
     echo "$msg";
     echo "</div>";
     echo "</div>";
    }
     ?>
    <form action="" method="post" enctype="multipart/form-data">
        <!-- Titulo-->
        <div class="form-group">
            <label for="exampleFormControlInput1">Título da obra</label>
            <input type="text" class="form-control" name="titulo" placeholder="<?php if(isset($_POST['titulo'])){echo $_POST["titulo"];}?>">
        </div>
        <!-- Categoria-->
        <div class="form-group">
            <label for="exampleFormControlSelect1">Categoria</label>
            <select class="form-control" name='categoria'>
                <option selected>Escolha uma categoria</option>
                <option>Anime</option>
                <option>Documentário</option>
                <option>Filme</option>
                <option>Livro</option>
                <option>Manga</option>
                <option>Seriado</option>

            </select>
        </div>
            <!-- Resenha-->
            <div class="form-group">
            <label for="exampleFormControlTextarea1">Resenha</label>
            <textarea class="form-control" name="resenha" rows="8" placeholder="<?php if(isset($_POST['resenha'])){echo $_POST["resenha"];}?>"></textarea>
        </div>
        <!-- Opinião-->
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="opiniao" value="Excelente">
            <label class="form-check-label" for="inlineRadio1">Excelente</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="opiniao" value="Muito bom">
            <label class="form-check-label" for="inlineRadio2">Muito bom</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="opiniao" value="Dá para terminar" >
            <label class="form-check-label" for="inlineRadio3">Dá para terminar</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="opiniao" value="Ruim" >
            <label class="form-check-label" for="inlineRadio4">Ruim</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="opiniao" value="Perdi meu tempo" >
            <label class="form-check-label" for="inlineRadio5">Perdi meu tempo</label>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" require name="capa">
            <label class="custom-file-label" for="customFile">Escolher arquivo</label>
        </div>
        <div class="col-auto my-1">
        <button type="submit" class="btn btn-primary" name="$pagina" value="Inserir Recomende">Enviar</button>
        </div>
        
    </form>
</div>

