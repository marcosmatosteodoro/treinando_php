<?php
require_once "paginas\Mysql/recomende_bd.php";
if(isset($_GET['id'])){
    
    $id = (int)$_GET['id'];

    $consulta = ver_recomende();

    foreach($consulta as $key=> $value){
        if ($value["id"] == $id){
            $titulo = $value["Titulo"];
            $resenha = $value["Resenha"];
            $categoria = $value["categoria"];
            $opiniao = $value["opiniao"];
            $img = $value["nome_img"];
            break;
        }
    }

}
?>

<div class='container'>
    <h1><?php echo $titulo ?></h1>
    <h4><?php echo $categoria ?></h4>
    <h4><?php echo $opiniao ?></h4>
    <div class='row'>
        <div class='col'>
            <p><?php echo $resenha ?></p>
        </div>
        <div class='col'>
            <img src=" upload/recomende/<?php echo $img ?>">
        </div>
    </div>
    <form>
        <button type="submit" name="$pagina" value="Recomende" class="btn btn-primary">Voltar</button>
        <button type="submit" name="$pagina" value="Alterar Recomende" class="btn btn-warning">Alterar</button>
    </form>
</div>