
<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="mt-5">Recomendações</h1><br>
        <table class="table">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Categoria</th>
              <th>Opiniao</th>
              <th>Resenha</th>
            </tr>
          </thead>
          <tbody>

          <?php
          require_once "paginas\Mysql/recomende_bd.php";
          $x = ver_recomende();
          foreach ($x as $Key => $value){
            if ($value['Mostrar'] != 1){
              continue;
            }

            $id = $value['id'];   
            $mostrar = $value['Mostrar'];   
            $titulo = $value['Titulo'];
            $resenha = $value['Resenha'];   
            $data_criacao = $value['data_criacao'];   
            $categoria = $value['categoria'];   
            $opiniao = $value['opiniao'];   
            $nome_img = $value['nome_img'];   

            echo "
              <tr>
                <td>
                  <a href='index.php?" . '$pagina' . "=Ver Recomende&id=$id' style=''><strong> $titulo</strong></a>
                </td>
                <td>$categoria</td>
                <td>$opiniao</td>
                <td><a href='index.php?" . '$pagina' . "=Ver Recomende&id=$id' type='submit' name= ".'$pagina' . " value='Ver Recomende'>Ver resenha</a></td>
              </tr>
            ";
          }
          ?>
              
          </tbody>
        </table>

      </div>
    </div>
    <div class="row">
      <div class="col-3">
        <form>
        <button type="submit" name="$pagina" value="Inserir Recomende" class="btn btn-primary">Faça uma recomendação</button>
        </form>
      </div>
      <div class="col">
        <nav aria-label="Page navigation example">
          <ul class="pagination">
            {% for pagina in dados.paginator.page_range %}
              {% if dados.number == pagina %}
               <li class="page-item active">
                 <a class="page-link" href="?p={{ pagina }}&termo={{ request.GET.termo}}">{{ pagina }}</a></li>
              {% else %}
                <li class="page-item">
                 <a class="page-link" href="?p={{ pagina }}&termo={{ request.GET.termo}}">{{ pagina }}</a></li>
              {% endif %}
            {% endfor %}
          </ul>
        </nav>
      </div>
    </div>
  </div>