<div class="container h-100" style="width: 70%; background-color:">
    <div class="col" style="max-width: 800px; min-width: 400px">
        <div class="card">
            <div class="card-header">
                <h1>Validador de CPF</h1>
            </div>
            <p style="margin: 1% 2% 0 2%">
                Neste projeto me propus a criar um simples programa que valida numericamente um CPF de acordo com os parâmetros
                do ministério da fazenda na intenção de exercitar os códigos<br>
                Vale ressaltar que não foi utilizado nenhum código já pronto para este projeto
            </p> 
            <hr>
            <div class="card-body">
                <form method="GET" action="">
                    <div class="mb-3">
                        <label  class="form-label">DIgite o CPF para validar.</label>
                        <input type="number" name="value"  value="<?php  if (isset($_GET['value'])){echo $_GET['value'];}; ?>" class="form-control mb-3">
                    </div>
                    <div style="margin-bottom: 10px">
                        <?php
                        require_once "paginas/funcoes/validador_e_gerador_CPF.php";
                        if(isset($_GET["value"])){
                            $value = (int)$_GET['value'];
                            $result = validaCPF($value);

                            echo "<div class='card' style='margin:0'>";

                            if($result){
                                echo "<div class='card-body btn-success text-center' >";
                                echo "$result";
                            }
                            else{
                                echo "<div class='card-body btn-danger text-center'>";
                                echo "CPF inválido";

                            }
                            echo "</div>";
                            echo "</div>";
                        }
                        ?>
                    </div>
                    <button type="submit" class="btn btn-primary"  name="$pagina" value="Validador de CPF">Validar</button>
                </form>

            </div>
        </div>
    </div>
    <br>
    <div style="max-width:800px">
        <h4 class="text-center" > Veja o código abaixo</h4>
        <div style="background-color:rgb(30,30,30);">    
            <img style='min-width:400px'src="vendor/img2/codigos/valida_cpf_img1.jpg">
            <img style='min-width:400px' src="vendor/img2/codigos/valida_cpf_img2.jpg">
        </div>
    </div>
</div>