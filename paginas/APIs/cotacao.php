<?php
echo "Vamos lá<br>";

$json_file = file_get_contents("https://economia.awesomeapi.com.br/last/USD-BRL,EUR-BRL,BTC-BRL");   
$json_str = json_decode($json_file, true);

$nome_dolar = $json_str["USDBRL"]["name"];
$alta_dolar = $json_str["USDBRL"]["high"];
$baixa_dolar = $json_str["USDBRL"]["low"];
$compra_dolar = $json_str["USDBRL"]["bid"];
$venda_dolar = $json_str["USDBRL"]["ask"];
$variacao_dolar = $json_str["USDBRL"]["varBid"];
$porcentagem_dolar = $json_str["USDBRL"]["pctChange"];
$data_dolar = $json_str["USDBRL"]["create_date"];

$nome_euro = $json_str["EURBRL"]["name"];
$alta_euro = $json_str["EURBRL"]["high"];
$baixa_euro = $json_str["EURBRL"]["low"];
$compra_euro = $json_str["EURBRL"]["bid"];
$venda_euro = $json_str["EURBRL"]["ask"];
$variacao_euro = $json_str["EURBRL"]["varBid"];
$porcentagem_euro = $json_str["EURBRL"]["pctChange"];
$data_euro = $json_str["EURBRL"]["create_date"];

$nome_bitcoins = $json_str["BTCBRL"]["name"];
$alta_bitcoins = $json_str["BTCBRL"]["high"];
$baixa_bitcoins = $json_str["BTCBRL"]["low"];
$compra_bitcoins = $json_str["BTCBRL"]["bid"];
$venda_bitcoins = $json_str["BTCBRL"]["ask"];
$variacao_bitcoins = $json_str["BTCBRL"]["varBid"];
$porcentagem_bitcoins = $json_str["BTCBRL"]["pctChange"];
$data_bitcoins = $json_str["BTCBRL"]["create_date"];

}
?>