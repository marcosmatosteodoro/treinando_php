<?php
function apenasnumeros($cnpj){
    $cnpj = str_replace(".", "", $cnpj);
    $cnpj = str_replace("/", "", $cnpj);
    $cnpj = str_replace("-", "", $cnpj);
    return $cnpj;
}

function sequencia($cnpj){
    $qt = strlen($cnpj);
    $seq = NULL;
    $i = 1;
    
    while ($i <= $qt){
        $seq = substr_replace($seq, $cnpj[0], 0, 0);
        $i ++;
    }
    if($seq == $cnpj){
        return true;
    }
    return false;
}

function calculadigito($cnpj, $parametro){
    $cont = array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
    $n = 0;
    $acumulador = 0;
    $i = 0;
    if ($parametro == 1){
        while (isset($cnpj[$n])){
            $acumulador += $cont[$n + 1] * $cnpj[$n];
            $n += 1;
        $digito = 11 - ($acumulador % 11);
        }
    }

    elseif ($parametro == 2){
        while (isset($cnpj[$n])){
            $acumulador += $cont[$n] * $cnpj[$n];
            $n += 1;
        $digito = 11 - ($acumulador % 11);
        }
    }

    if ($digito > 9){
        $digito = 0;
    }
    return $cnpj . $digito;
}

function formataCNPJ($cnpj){
    $cnpj = substr_replace($cnpj, "-", 12 , 0);
    $cnpj = substr_replace($cnpj, "/", 8, 0);
    $cnpj = substr_replace($cnpj, ".", 5, 0);
    $cnpj = substr_replace($cnpj, ".", 2, 0);

    return $cnpj;
}

function validaCNPJ($cnpj){
    $cnpj = apenasnumeros($cnpj);
    if (sequencia($cnpj)){
        return false;
    }
    if (strlen($cnpj) != 14){
        return false;
    }
    $cnpjnovo = substr($cnpj, 0, -2);
    $cnpjnovo = calculadigito($cnpjnovo, 1);
    $cnpjnovo = calculadigito($cnpjnovo, 2);

    if ($cnpj != $cnpjnovo){
        return false;
    }
    return "CNPJ: $cnpj válido";
}

function geraCNPJ($formata=false){
    $cnpjgerado = (string)rand(10_000_000, 99_999_999);
    $cnpjgerado = $cnpjgerado . "0001";

    $cnpjgerado = calculadigito($cnpjgerado , 1);
    $cnpjgerado = calculadigito($cnpjgerado , 2);

    if ($formata == 1){
        return formataCNPJ($cnpjgerado);
    }

    return $cnpjgerado;
}
?>