<?php
function apenasNumeros($cpf){
    $cpf = str_replace("-", "", $cpf);
    $cpf = str_replace(".", "", $cpf);
    return $cpf;
}

function sequencia($cpf){
    $qt = strlen($cpf);
    $i = 1;
    $seq = NULL;
    while($i <= $qt){
        $seq = substr_replace($seq, $cpf[0], 0, 0);
        $i ++;
    }
    if($seq == $cpf){
        return true;
    }
    return false;
}

function calculaDigito($cpf, $parametro){
    $cont = array(11,10,9,8,7,6,5,4,3,2);
    $acumulador = 0;
    $n = 0;
    if ($parametro == 1){
        while(isset($cont[$n+1])){
            $acumulador += $cpf[$n] * $cont[$n+1];
            $n +=1;
        }
    }
    elseif ($parametro == 2){
        while(isset($cont[$n])){
            $acumulador +=$cpf[$n] * $cont[$n];
            $n +=1;
        }
    }
    $digito = 11 - ($acumulador % 11);
    if($digito > 9){
        $digito = 0;
    }
    $cpf = $cpf . $digito;
    return $cpf;    
}

function validaCPF($cpf){
    $cpf = apenasNumeros($cpf);
    if (sequencia($cpf)){
        return false;       
    }
    if (strlen($cpf) != 11){
        return false;
    }
    $cpfnovo = substr($cpf, 0, -2);
    $cpfnovo = calculaDigito($cpfnovo, 1);
    $cpfnovo = calculaDigito($cpfnovo, 2);

    if ($cpf != $cpfnovo){
        return false;
    }
    return "CPF: $cpf é válido";
}

function geraCPF($formata=false){
    $cpfgerado = (string)rand(100_000_000, 999_999_999);
    
    $cpfgerado = calculaDigito($cpfgerado, 1);
    $cpfgerado = calculaDigito($cpfgerado, 2);

    if ($formata){
        $cpfgerado = substr_replace($cpfgerado,"-", 9, 0);
        $cpfgerado = substr_replace($cpfgerado,".", 6, 0);
        $cpfgerado = substr_replace($cpfgerado,".", 3, 0);
    }
    return $cpfgerado;
}
?>